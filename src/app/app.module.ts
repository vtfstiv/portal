import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {LoginModule} from './components/login/login.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {HttpInterceptor} from './interceptor/http.interceptor';
import {CanActivatePage} from './security/can-activate';
import {NavToolbarComponent} from './components/toolbar/nav-toolbar.component';
import {MatToolbarModule} from '@angular/material/toolbar';
import {FlexLayoutModule} from '@angular/flex-layout';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {CanActivateAdmin} from './security/can-activate-admin';
import {SnackBarService} from './common-services/snack-bar.service';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {HttpErrorInterceptor} from './interceptor/http-error.interceptor';
import {MatBadgeModule} from '@angular/material/badge';
import {HomeComponent} from './components/home/home.component';

@NgModule({
  declarations: [
    AppComponent,
    NavToolbarComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LoginModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatSnackBarModule,
    MatBadgeModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    CanActivatePage,
    CanActivateAdmin,
    SnackBarService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

import {Component, Inject, OnInit} from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from '@angular/material/dialog';
import {Product} from '../../../../../models/product.model';
import {SupplierService} from '../../../supplier/supplier.service';
import {Supplier} from '../../../../../models/supplier.model';

@Component({
  selector: 'app-edit-product',
  templateUrl: './edit-product.component.html',
  styleUrls: ['./edit-product.component.scss']
})
export class EditProductComponent implements OnInit {

  product: Product;
  suppliers: Supplier[];
  selectedItemName: string;
  base64Image: string;

  constructor(
    public dialogRef: MatDialogRef<EditProductComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    protected supplierService: SupplierService) {
  }

  ngOnInit(): void {
    this.product = this.data.product;
    this.getSuppliers();
  }

  getSuppliers(): void {
    this.supplierService.getSuppliers(0, 1, true).subscribe(res => {
      this.suppliers = res.body;
    });
  }

  compareObjects(o1: any, o2: any): boolean {
    return o1?.name === o2?.name && o1?.id === o2?.id;
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  onFileSelected(fileInput): void {
    const file: File = fileInput.target.files[0];
    this.selectedItemName = file.name;
  }

  removeSelectedFile(): void {
    this.selectedItemName = null;
  }
}

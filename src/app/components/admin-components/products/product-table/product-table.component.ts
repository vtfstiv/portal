import {Component, OnInit, ViewChild} from '@angular/core';
import {Product} from '../../../../models/product.model';
import {PageEvent} from '@angular/material/paginator';
import {ProductService} from '../product.service';
import {MatDialog} from '@angular/material/dialog';
import {SnackBarService} from '../../../../common-services/snack-bar.service';
import {EditProductComponent} from '../dialogs/edit-product/edit-product.component';
import {DeleteSupplierComponent} from "../../supplier/dialogs/delete-supplier/delete-supplier.component";
import {DeleteProductComponent} from "../dialogs/delete-product/delete-product.component";

@Component({
  selector: 'app-product-list',
  templateUrl: './product-table.component.html',
  styleUrls: ['./product-table.component.scss']
})
export class ProductTableComponent implements OnInit {

  @ViewChild('productTable') table: any;
  products: Product[];
  loading = false;
  totalElements: any = 0;
  pageSize: any = 10;
  page: any = 0;


  constructor(protected productService: ProductService,
              protected dialog: MatDialog,
              protected snackbarService: SnackBarService) {
  }

  async ngOnInit(): Promise<void> {
    this.getProducts();
  }

  getProducts(): void {
    this.loading = true;
    this.productService.getProducts(this.page, this.pageSize).subscribe(products => {
      this.totalElements = products.headers.get('X-Total-Count');
      this.loading = false;
      this.products = products.body;
    });
  }

  editProduct(product?: Product): void {
    const toBeChange = Object.assign({}, product);
    const dialogRef = this.dialog.open(EditProductComponent, {
      data:
        {
          product: toBeChange,
          isEditing: product ?? false
        }
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.productService.saveProduct(result).subscribe(res => {
          this.snackbarService.openSnackBar('Actiunea a avut succes', 'OK', false);
          this.getProducts();
        });
      }
    });
  }

  deleteProduct(product: Product): void {
    const productToDelete = Object.assign({}, product);

    const dialogRef = this.dialog.open(DeleteProductComponent, {
      data: {product: productToDelete}
    });

    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        this.productService.deleteProduct(res).subscribe(result => {
          this.snackbarService.openSnackBar('Actiunea a avut succes', 'OK', false);
          this.getProducts();
        });
      }
    });
  }

  changePage(pageEvent: PageEvent): void {
    this.page = pageEvent.pageIndex;

    this.getProducts();
  }
}

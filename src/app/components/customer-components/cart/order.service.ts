import {Injectable} from '@angular/core';
import {HttpClient, HttpParams, HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Order} from '../../../models/order.model';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  private readonly orderUrl: string = 'order';

  constructor(private httpClient: HttpClient) {
  }

  placeOrder(order: Order): Observable<Order> {
    return this.httpClient.post<Order>(this.orderUrl + '/placeOrder', order);
  }

  getOrders(pageNumber, pageSize): Observable<HttpResponse<Order[]>> {
    let params = new HttpParams();
    params = params.set('pageNumber', pageNumber);
    params = params.set('pageSize', pageSize);

    return this.httpClient.get<Order[]>(this.orderUrl, {params, observe: 'response'});
  }
}

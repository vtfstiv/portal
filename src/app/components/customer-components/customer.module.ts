import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RouterModule, Routes} from '@angular/router';
import {CanActivatePage} from '../../security/can-activate';
import {PersonalProfileComponent} from './personal-profile/personal-profile.component';
import {MatCardModule} from '@angular/material/card';
import {MatInputModule} from '@angular/material/input';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgxMaskModule} from 'ngx-mask';
import {FlexModule} from '@angular/flex-layout';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatTableModule} from '@angular/material/table';
import {ProductsListComponent} from './products-list/products-list.component';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {CartComponent} from './cart/cart.component';
import {MatPaginatorModule} from '@angular/material/paginator';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import {MatDialogModule} from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import { AddressComponent } from './address/address.component';
import { AddressDialogComponent } from './address/dialog/address-dialog/address-dialog.component';
import {MatListModule} from '@angular/material/list';
import {MatRadioModule} from '@angular/material/radio';
import {AddressChooserComponent} from './address/dialog/address-chooser/address-chooser.component';
import { OrdersComponent } from './orders/orders.component';
import { OrderDetailsComponent } from './orders/dialog/order-details/order-details.component';

const routes: Routes = [
  {
    path: 'profile',
    component: PersonalProfileComponent,
    canActivate: [CanActivatePage]
  },
  {
    path: 'products',
    component: ProductsListComponent,
    canActivate: [CanActivatePage]
  },
  {
    path: 'cart',
    component: CartComponent,
    canActivate: [CanActivatePage]
  }
];

@NgModule({
  declarations: [PersonalProfileComponent, ProductsListComponent, CartComponent, AddressChooserComponent, AddressComponent, AddressDialogComponent, OrdersComponent, OrderDetailsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatCardModule,
    MatInputModule,
    ReactiveFormsModule,
    NgxMaskModule,
    FlexModule,
    MatExpansionModule,
    MatGridListModule,
    MatTableModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    NgxDatatableModule,
    MatDialogModule,
    MatTooltipModule,
    MatListModule,
    FormsModule,
    MatRadioModule
  ]
})
export class CustomerModule {
}

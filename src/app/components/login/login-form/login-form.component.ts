import {Component, OnInit} from '@angular/core';
import {AuthenticationService} from '../authentication.service';
import {Customer} from '../../../models/customer.model';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from "@angular/router";
import {first} from "rxjs/operators";
import {Authorization} from "../../../models/enums/authorization.enum";

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {

  errorMessage: string;
  customer: Customer;
  customerFormGroup: FormGroup;

  constructor(private authenticationService: AuthenticationService,
              private formBuilder: FormBuilder,
              private router: Router,
              private authService: AuthenticationService) {
  }

  ngOnInit(): void {
    if (this.authService.currentCustomerValue) {
      if (this.authService.currentCustomerValue.authority === Authorization.ADMIN) {
        this.router.navigate(['admin/warehouse']);
      } else {
        this.router.navigate(['/welcome']);
      }
    }
    this.customerFormGroup = this.formBuilder.group(
      {
        email: ['', Validators.required],
        password: ['', Validators.required]
      }
    );
  }

  loginCustomer(): void {
    this.customer = new Customer(this.customerFormGroup.value);
    this.authenticationService.login(this.customer).pipe(first())
      .subscribe(
        data => {
          if (data.authority === Authorization.ADMIN) {
            this.router.navigate(['admin/warehouse']);
          } else {
            this.router.navigate(['/welcome']);
          }

        },
        error => {
          this.errorMessage = error?.error?.message;
        });
  }
}

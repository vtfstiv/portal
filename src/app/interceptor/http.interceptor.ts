import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {AuthenticationService} from '../components/login/authentication.service';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {

  readonly baseUrl = 'http://localhost:8090/awbd/';

  constructor(private authenticationService: AuthenticationService) {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    const currentCustomer = JSON.parse(sessionStorage.getItem('currentCustomer'));
    if (currentCustomer && currentCustomer.token) {
      request = request.clone({
        setHeaders: {
          Authorization: `Bearer ${currentCustomer.token}`
        }
      });
    }

    const clonedUrl = request.clone({
      url: this.baseUrl + request.url
    });
    return next.handle(clonedUrl);
  }
}

import {Supplier} from './supplier.model';

export class Product {
  id?: string;
  productName?: string;
  price?: number;
  description?: string;
  productSupplier?: Supplier;
  image?: File;

  constructor(init?: Partial<Product>) {
    Object.assign(this, init);
  }

}
